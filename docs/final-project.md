# PROJET FINAL   

## Enoncé   
Pour ce projet final il s’agit de se réapproprier l’objet choisi au ADAM Museum pour le réinterpréter en fonction d’un des cinq mots qui nous a été donné. Les mots en question sont : **Référence**, **Influence**, **Inspiration**, **Extension** et **Hommage**.
Pour en comprendre le sens la définition de chaque mot a été reprise par quatre dictionnaires d’époques différentes : Wiktionnary, Le Petit Robert, Larousse et Littré.  


## Définitions de l’inspiration  

**Wiktionnary**  

_Inspiration \ɛɑs.pi.ʁa.sjɔɑ\ féminin
• (Physiologie) Action par laquelle l’air entre dans le poumon ; mouvement opposé à l’expiration. 
• (Par analogie) Sorte de souffle divin qui pousse à tel ou tel acte. • (Par extension) Acte de stimulation de l’intellect, des émotions et de la créativité à partir d’une **influence**. Idée créatrice, élan créateur d'origine mystérieuse. Je manque d'inspiration. L'inspiration ne vient pas. Céder à l’inspiration. On sent dans cette poésie la chaleur de l’inspiration.
Cette faculté singulière, toujours dominante et jamais soumise, inégale, indisciplinable, impitoyable, venant à son heure et s’en allant comme elle était venue, ressemblait, à s’y méprendre, à ce que les poëtes nomment l’inspiration et personnifient dans la Muse. — (Eugène Fromentin, Dominique, L. Hachette et Cie, 1863, réédition Gründ, page 92) Dans le secret de ton être peut se manifester l'inspiration. Ce mot, « inspiration », vient du l atin in spiritu : cela signifie que tu es alors immergé dans la profondeur de ton propre esprit. En cet instant privilégié s'entremêlent l'intuition, le sentiment, l'émotion et la joie. Tu as alors rendez-vous avec ta créativité. — (Jean Proulx, Grandir en humanité, Fides, 2018, page 54).  

**Le Petit Robert**  

Inspiration
• Souffle créateur qui anime les artistes, les chercheurs. L'inspiration poétique. Attendre l'inspiration. • Action d'inspirer, de conseiller qqch. à qqn ; résultat de cette action.  **Influence**, ➙ instigation. • Idée, résolution spontanée, soudaine. Une heureuse inspiration. • PHYSIOLOGIE Action par laquelle l'air entre dans les poumons ; résultat de cette action. ➙ aspiration. 
**Larousse**
Inspiration
•Phase de la respiration pendant laquelle l'air atmosphérique, riche en oxygène, pénètre dans les poumons. •Mouvement intérieur, impulsion qui porte à faire, à suggérer ou à conseiller quelque action : Suivre son inspiration. •Enthousiasme, souffle créateur qui anime l'écrivain, l'artiste, le chercheur : Chercher l'inspiration. •Ce qui est ainsi inspiré : De géniales inspirations. •Influence exercée sur un auteur, sur une œuvre : Une décoration d'inspiration crétoise. •**Influence** charismatique de Dieu sur les auteurs des livres saints  

**Littré**  

Inspiration (in-spi-ra-sion ; en vers, de cinq syllabes)s. f. •1Terme de physiologie. Action par laquelle l'air est inspiré, entre dans les poumons, mouvement opposé à l'expiration. Immédiatement après que l'air, chargé d'exhalaisons nuisibles, a été chassé au dehors par l'expiration, l'inspiration introduit dans le poumon un nouvel air, et avec lui bien des principes qui influent plus ou moins sur la sanguification, Bonnet, Contempl. nat. Œuvr. t. VIII, p. 30, note 1, dans POUGENS •3Particulièrement.L'enthousiasme qui entraîne les poètes, les musiciens, les peintres. Ce vers a été écrit d'inspiration. Cette poésie est pleine d'inspiration. •4Action de conseiller quelqu'un, de lui suggérer quelque chose.  


> J’ai supprimé des parties de définitions qui n’était pas pertinentes pour moi.  


## Les raisons du choix  

Vous l’aurez compris mon choix est l’inspiration. Cependant, j’ai hésité entre l’_inspiration_ et _l’Influence_ qui sont assez proche. On retrouve influencé pour la définition d’inspiration dans les quatre dictionnaires
L’influence a un caractère plus cadré, limité dans la création comme si l’on suivait un seul chemin. J’ai donc l’impression qu’a un moment je me retrouverai bloqué. 
J’ai finalement opté pour l’inspiration qui a cette dimension plus libre et ouverte correspondant plus à ma manière de fonctionner. C’est d’ailleurs par inspiration que j’ai trouvé les idées pour l’exercice de la lampe en polypropylène. L’inspiration vient généralement de tout ce qui nous entoure de tout ce qui va stimuler nos sens pour faire germer des idées qui vont se connecter entre elles.  

Comme pour l’exercice de la lampe je me suis mis devant une feuille blanche et j’ai essayé de représenter ce qui me passait par la tête. Ce n’est pas venu le premier jour ni le deuxième car je cherchais trop quelque chose de précis. J’étais bloqué car j’allais dans une seule et même direction.
C’est également cela qui m’a fait choisir l’inspiration au lieu de l’influence je trouve que l’influence nous laisse moins de liberté justement on est focus sur une seule chose on a un but précis indiqué tandis que l’inspiration pas. La liberté est plus grande dans l’inspiration et ne doit pas seulement venir d’une chose. En me mettant devant ma feuille blanche je me suis imposé l’exercice je devais trouver absolument. J’ai donc l’impression que je cherchais l’influence que la chaise pouvait avoir sur moi. Cependant je ne fonctionne pas ainsi c’est peut-être pour cela que rien ne venait.
La plupart du temps où j’ai trouvé des idées c’était en n’y pensant pas directement. L’idée venait puis je la notais/dessinais. Ensuite par curiosité je regardais ce qui existais déjà.  

Si je devais schématiser mon inspiration je dirais que c’est comme un feu d‘artifice. Ce sont plusieurs éléments qui assemblé ensemble forment un ensemble organisé qui lors de l’explosion révèle la création finale reprenant les idées en formant un tout.  
![](./images/unamed.gif)

J’ai repris la définition **Wikipédia** d’un feu d’artifice dont j’ai modifié les termes pour en faire une définition de l’inspiration. 
« Un feu d'artifice est un procédé pyrotechnique utilisant des explosifs déflagrants visant à produire du son, de la lumière et de la fumée à l'aide d'une composition pyrotechnique. »  

Une **inspiration** est un procédé **de stimulations mentales** qui utilise la **création** en visant la production d’**émotion**, à l’aide d’une composition d’**idées**.  

L'inspiration est quelque chose qui vient assez soudainement chez moi il ne faut pas la brusquer. Je l’attends toujours mais ce n'est qu'une question de temps.  
    
WORK IN PROGRESS ...  


## Les premières idées  

Mon idée de départ était de reprendre le principe du fauteuil à bascule Dondolo de Cesare Leonardi et Franca Stagi qui pour moi est un dérivé du rocking chair.  
Cette emblématiue chaise à bascule fut conçu pour la première fois en Angleterre existe au 18e siècle. D'après la rumeur elle aurait été inventé par Benjamin Franklin. Ce n'est qu'au 19e siècle que le rocking chair sera produit industriellement grace à une technique permettant de courber le bois avec la chaleur inventé par Michael Thonet. 
Au fil des années le siège a été revisité dans tous les sens utilisant toutes sortes de matériaux
![](./images/fauteuilpic.jpg) ![](./images/rocking_chair.jpg)  

Je ne voulais pas me limiter à reproduire une chaise tel quel.   
En revoyant l’objet la première chose qui m’est venu à l’esprit c’est le **balancement**. A ce moment-là je me suis remémoré des souvenirs d’enfance. Débordant d’énergie à la limite de l’hyperactivité je ne pouvais pas rester sans bouger sur ma chaise durant les cours. J’ai donc pris la mauvaise habitude de me balancer sur ma chaise. C’était une façon pour moi de canaliser mon énergie ou d’évacuer mon stresse. 
Pour moi le balancement à un coté apaisant et relaxant on peut également faire le lien avec le bercement.
Malgré sa finesse et sa légèreté le Dondolo est quand même assez imposant. Encore une fois un flash m’est venu avec cette image du rocking chair posé sur le porche d’une maison ou au coin d’un feu. En soi toujours associé à la maison ce que je trouvais dommage. 
J’ai donc pensé à une façon de le déplacer dans un autre espace 
J’ai donc pensé à une version portable du rocking chair.
De fil en aiguille j’ai réfléchi à une manière efficace de le transporter en le repensant sous forme de sac à dos. Mais je ne veux pas seulement d’une chaise transportable, je voudrais qu’une fois que celle-ci est refermée qu’elle puisse aussi contenir des choses.  

J'ai repris différents modèles pour les comparés et voir les points communs 

![](./images/a4.jpg)
 ![](./images/traces.jpg)


### Prototype 1  

Pour mon premier prototype j’ai imaginé une chaise pliable avec un système de charnière dentelés ou pivotantes reliant le siège, le dossier et les pieds. Ici je voulais voir comment les différentes pièces allaient fonctionner ensemble pour avoir une base en ne prenant pas compte de la partie contenant pour l’instant.
J’ai modélisé la chaise sur fusion 360 en prenant comme base une forme elliptique avec les mesures de mon sac à dos en me disant qu’elle allait assurer le balancement. J'ai ensuite utilisé Prusa slicer pour imprimer les différents éléments en 3D.

Les photos montrent les différentes parties.  

![](./images/pièces1.jpg)
![](./images/partigd.jpg)  

![](./images/dossiera.jpg)
![](./images/siège.jpg)  

![](./images/char.jpg)  ![](./images/cha2.jpg)  


Lors de l’assemblage j’ai rencontré quatre problèmes. 
Le premier c’est que la forme ressemblait à une lunette de toilette.
Le deuxième est qu’il fallait prendre en compte l’espacement entre chaque dentelure pour que les charnières puissent s’emboiter ce que je n’ai pas fait. Je n’ai pas laissé assez de jeu par conséquent l’emboitement se faisait difficilement.  

![](./images/emboit1.jpg)
![](./images/emboit2.jpg)  

J’ai donc dû refaire une version avec des espacements légèrement plus grands. Ici la différence d’écart fait au niveau de la partie siège.  

![](./images/av.jpg)
![](./images/ap.jpg)  

Le troisième souci était que mes dentelures devant accueillir la charnière étaient beaucoup trop fines et donc elles ont rompu lors de l’assemblage.  

![](./images/crac1.jpg)
![](./images/crac_2_.jpg)  

J’ai tout de même tenté de voir ce que ça donnait en assemblant le tout avec du scotch. Étant trop concentré sur le balancement je n’ai pas tenu compte de comment allaient fonctionner les éléments de la chaise une fois assemblé.  

**EXPECTATION**  

![](./images/expectation.jpg)  

Par conséquent le balancement se faisait soit vers l’avant soit vers l’arrière le poids n’était pas du tout bien répartie. J’ai placé le siège au centre des deux pieds il n’y avait donc aucun équilibre. J’aurais dû les mettre à l’extrémité avant et penché en arrière en plus de ça mes pieds avaient une dimension plus haute que longue qui rend le tout instable.  

**REALITY**  

![](./images/reality.gif)  

Voici ce que ça donne lorsque la chaise est repliée.  

![](./images/repli.jpg)  

Ce premier prototype est un échec.  


J’ai reparlé de mes idées avec Laurens et Gwendoline lors de mon passage au FabLab. Ils m’ont donné quelques pistes à explorer notamment pour la partie contenant du dondobag. Il me fallait un matériau à la fois souple mais aussi solide. Gwendoline m’a parlé du plastic bag upcycle qui consiste à reprendre des sacs plastiques qu’on superpose et qu'on presse à chaud pour avoir comme un tissu plastique résistant mais souple. 

### Prototype 2  

A la suite de cette discussion en est ressorti l’idée de chaise gonflable. De cette façon mon contenant pouvait être à la fois souple lorsqu’il est dégonflé et rigide quand il est gonflé. J’ai donc essayé de combiner cette idée aux autres que j’avais déjà.
Pour développer la conception de mon objet j’ai fait des recherches sur la méthode utilisée pour confectionner les canoés gonflables car j’ai déjà vu des sacs à dos fabriqués à partir de toiles de kayak recyclé. Je suis tombé sur une chaine youtube [DIY Packraft ]( https://www.youtube.com/watch?v=_iYYXfNHm0c&ab_channel=DIYPackraft). Les méthodes d’assemblage utilisés sont plus ou moins similaires. Il utilise un fer chaud pour que les faces des tissus adhèrent entre elles.  

J’ai regardé les étapes à suivre pour le plastic upcycling et j’ai trouvé une vidéo explicative d’un fablab se trouvant aux philippines. Les étapes consistent à récupérer des sacs plastiques qu’il faut découper en morceaux puis disposer entre deux feuilles de papier cuissons qui seront ensuite presser à chaud. A la fin on obtient une feuille plastique. Lien de la vidéo : [JICA ICT and Development] (https://www.youtube.com/watch?v=kDmFIPYNfnI&ab_channel=JICAICTandDevelopment)  

Vu que le fauteuil doit être gonflé je regardé le dispositif manuel de gonflage des gilets de sauvetages. Il s’agit d’un mécanisme de percussion hydrostatique avec cartouche de gaz.  En résumé, en actionnant la tirette le percuteur de la cartouche va être libérer et le gaz va gonfler le gilet.  

![](./images/percu.jpg)  

J’ai d’abord découpé des pieds en cartons pour avoir une structure que j’ai fait tenir debout avec du ruban tesa. Cela m’a permis de simuler une assise et d’exercer une charge au niveau de l’assise pour voir comment allait se comporter les deux parties.  

![](./images/découpe.jpg)  

Les deux pieds ont fléchi vers le centre.  

![](./images/assemblaage.jpg)

Ensuite j’ai fait différent test en découpant dans des plastiques usés. J’ai commencé par faire une forme simple de ballon que j’ai refermé sur le bord en utilisant mon fer à repasser et du papier cuisson en laissant un trou pour pouvoir le gonfler.  

![](./images/ballon.jpg)  

![](./images/fer.jpg)  


J’ai gonflé le ballon à l’aide de ma pompe électrique pour voir comment se comportait le plastique.  

![](./images/pompe.jpg)  

Vu que j’ai simplement découper une forme à plat le résultat obtenu était assez fin.  


J’ai découpé d’autres gabarits en traçant les courbes des pieds du fauteuil. J’espérais qu’une fois gonfler le plastique allait épouser la forme des deux pieds et contreventer la structure mais je n’avais que du ruban tesa pour fixer le tout ce qui n’était pas pratique.  

![](./images/shpae.jpg)  

![](./images/ballon_2_.jpg )  

 ![](./images/gonflage__2_.gif)  


La prochaine étape était d’imprimer les pièces en 3D sur lesquels je pourrais fixer la partie contenante et de créer un siège à l’aide des sacs plastiques qui se placerait entre les deux structures.  

**Correction** :   

La correction à mi en lumière des éléments à revoir. Il s’avère que par rapport au premier prototype je me suis dirigé vers quelque chose de trop complexe et qui manquait d’ergonomie. Il a donc fallu faire un choix entre le système gonflable et dépliant. Sachant que le système gonflable nécessite une pompe pour ne pas s’épuiser cela n’était finalement pas pratique. Pour éviter de s’encombrer j’ai opté pour la solution de départ avec un fauteuil pliable. Néanmoins, je vais essayer de continuer avec la méthode de plastic upcycle pour une partie du sac et si possible envisager que la chaise fasse office de contenant quand elle ne sert pas d’assise.


### Prototype 3  

Pour ce prototype j’ai repris les dimensions d’un sac à dos de randonné trekking pour avoir des dimensions assez grandes pour confectionner un rocking chair portable confortable. De plus c’est en lien avec ma volonté de pouvoir l’utiliser dans n’importe quel endroit.  

  ![](./images/dimensions-sac-a-dos-osprey-exos-48.jpg)  

J’ai commencé par modéliser les parties dures du dondobag sur fusion 360. L’idée est ici que les pieds de la chaise formeraient les parties latérales du sac et que le siège soit la partie rigide servant d’appui dorsal. Des bras pouvant pivoter sont intégrés dans la forme du piétement.
L’ensemble est recouvert par une toile faite de plastique recyclé suivant la méthode de la vidéo que l’on retrouve dans l’explication du deuxième prototype.  

#### Les différentes étapes à suivre pour obtenir la toile en plastique :  

1)	Récupérer des sacs plastiques qui ne servent plus  

 ![](./images/sacs.jpg)  

2)	Découper  

 ![](./images/lambeaux.jpg)  

3)	Presser les copeaux entre deux feuilles de papier cuisson avec un fer à repasser. Ainsi le plastique va fondre et les différents morceaux vont fusionner ensemble.  

 ![](./images/ toile.jpg)  


Le plastique permet d’avoir une couverture étanche et souple.
La toile vient se fixer à la base du sac à dos uniquement. Le reste est accroché à des bandes velcro qui permettent d’accéder facilement à la partie intérieure. Celles-ci servent également à faire adhérer la toile aux bras servant à pour former le dossier lors du passage au mode rocking chair.

 ![](./images/face_arrière.jpg)
 ![](./images/face_avant.jpg)
 ![](./images/ouverture.jpg)
 ![](./images/dossier_relevé_.jpg)

## Prototype 4   

#### Test 1  

Je suis revenu sur un prototype gonflable à la suite de la deuxième correction. J’ai fait un test et essayé premièrement d’avoir une forme cubique pour avoir un volume avec des cotés plus définis contrairement aux tests effectués pour le prototype 2. 
Mon cube dont que j’ai tracé six faces et ensuite découpé en veillant à laisser des marges sur les côtés de chaque carré pour me permettre de les coller ensemble.  

 ![](./images/plast.jpg )  

![](./images/facess.jpg)  


 
Pour se faire j’ai utilisé comme matériel : un fer à repasser, du papier cuisson, un poste à souder et du plastique récupéré.
J’ai commencé à coller les faces avec le fer à repasser mais j’ai ensuite utilisé un poste à souder car le fer était trop volumineux pour faire les coins une fois que certaines faces étaient collées. Le problème de mon poste à souder était que je ne pouvais pas régler la température donc le plastique fondait assez vite.  

![](./images/fsou.jpg)   ![](./images/soud.jpg)  ![](./images/collage.jpg)  

J’ai également utilisé un morceau de carton pour m’aider à refermer les arêtes de mon cube. Ainsi je pouvais mettre deux faces avec le carton entre et éviter qu’elle ne se colle entre elle.
Je n’ai pas réussi à récupérer le carton.  

![](./images/ cube.jpg )  

Voilà le résultat.  

![](./images/vidcube.gif)  


#### Test 2  

Mon quatrième prototype est conçu avec une assise qui se gonfle et de pieds en forme d’ellipse en dur. 
Pour le premier essaie j’ai utilisé un sac poubelle et j’ai découpé les ellipses dans du carton. L’idée était d’intégrer les parties en cartons en soudant les plastique autour. Il y aurait une partie se retrouvant à l’intérieur et une autre partie qui viendrait s’ouvrir en pivotant à l’extérieur pour réduire la profondeur du sac à dos. J’ai fait exactement pareil que pour le cube pour coller les différentes parties entre elles.   

1)	Découpe des pieds  

![](./images/scarton.jpg) ![](./images/rés.jpg)  

2)	Tracer sur le sac  

![](./images/traacc.jpg)  

3)	 Découpage des pièces  

![](./images/cut.jpg)  

4)	Intégration de la moitié du pied  

![](./images/inte.jpg)  

5)	Fixation de tendeur pur limité le gonflement de la chaise et lui donner sa forme  

![](./images/tend.jpg)  


Je n’ai pas réussi à terminer cet essaie car je n’ai pas fait le chose dans le bon ordre. J’aurais du d’abord assembler les différentes parties constituant l’assise avant d’y intégrer les pieds.  Par conséquent à une certaines étapes le poids des pieds ne me permettait plus de manipuler l’objet comme je voulais. Au final deux faces plastiques sont entrées en contact et on fusionner ce qui à bloqué tout le processus car ce n’était plus récupérable.  



#### Test 3  

Le test 2 était un peu brouillon…
Pour ce troisième test j’ai dessiné sur autocad un patron comme on fait en couture avec les différentes parties qui compose le fauteuil. J’utilise à nouveau des sacs poubelle pour avoir une structure gonflable.  

![](./images/pattern.JPG)  


**TUTORIAL**  

Vous aurez besoin de : papier cuisson, sac plastique, poste de soudure, ruban adhésif, carton  

1)	Pour une découpe nette et précise utiliser la lasersaure du fablab. Pour éviter de déclencher un feu ou faire fondre le plastique j’ai mi la vitesse à 2300 et une puissance assez faible de 20 %. Le premier test était le bon.
Voici ce que ça donne.  

![](./images/deoupp.gif )  

2)	Munissez-vous d’un poste à souder (dont vous pouvez régler la température)
Là, pareil j’ai effectué des tests pour voir à quelle température je pouvais faire des soudures nettes. La bonne température semble être 215°.  

 ![](./images/com.jpg) 
![](./images/goot.jpg)  

3)	Tracez des repères le long de la marge.  

![](./images/rep.jpg)  

4)	Faites correspondre les repères entre en superposant les faces  

 ![](./images/supe.jpg)  

5)	Aidez vous d’un carton et de ruban adhésif pour fixer les faces à souder pour éviter un décalage. Le carton permet de faire les angles et d’avoir ses faces à plat sans risquer lors de la soudure de coller les parties en dessous avec.  

 ![](./images/carton__2_.jpg)  

6)	Collez les différentes paties sans oublier de laiser un espacepour intégrer la valve  

7)	Prenez un objet vous permettant de faire la valve (un porte mine par exemple)  

Ensuite soudez le long de celui-ci.  

 ![](./images/mine.jpg )
![](./images/valve.jpg)  


J’ai utilisé le reste d’un stylo pour que la pipette de la pompe électrique soit immobiliser.  

![](./images/stylo.jpg )  


8)	À l’aide d’une pompe gonfler le fauteuil  


Résultat :  

 ![](./images/resulti.jpg)  




Je n’ai pas développé le coté portatif ni contenant du sac à dos car ici le prototype sert à se rendre compte des capacités du sac poubelle.  Il faut savoir que les matériaux normalement utilisés pour ce type de structure gonflable sont : le PVC, le nylon, le velours côtelé, le feutre ou du plastique plus résistant. 
L’idée reste d’utilisé des plastique qu’il faut recycler en objet. J’ai pensé à utiliser la méthode du prototype 3 (upcycling bag) mais le résultat démontre qui n’est pas possible d’en faire quelque chose de gonflable car la finition n’était pas optimale. En effet, devient trop rigide après la presse des morceaux de plastiques. De plus, il reste des imperfections tel que des trous laisseraient passer l’air.

J’ai modélisé des demi-ellipses avec un système de pivots. 
Celles-ci se présentent en trois parties, supérieure, centrale et inférieure. Les parties inférieures et supérieures sont fixes et pour retourner à l’idée du sac à dos représenterais le dos du sac et la partie centrale les pied qui pivoteraient vers le bas. Ainsi, le volume du dondobag ne sera pas démesuré.  
 
![](./images/proto_4_v20.png)
![](./images/DEMO_PROTO_4_v1.png)

Je n’ai pas pu finaliser le prototype car j’ai rencontré un problème lors de l’impression. Etant donné que mon impression 3D durait plus de 18 heures je n’ai pas eu le temps de recommencer.  

 ![](./images/crash.jpg)  

Ci-dessous des esquisses qui illustrent le l’idée.  
 
![](./images/d1.jpg)
![](./images/IMG_9372__1_.jpg)


## Projet final  

![](./images/projet_final.jpg)  
  
Le Dondolo m’a procuré un sentiment de relaxation généré par ses courbes mettant en scène le mouvement au sens propre comme au figuré. C’est de cette sensation que les premières idées me sont apparues. En analysant le fauteuil à bascule j’ai voulu repenser certains aspects. Les premières idées étant d’en faire une chaise de plus petit gabarit, qu’elle soit portable et qu’on puisse l’utiliser comme contenant à la manière d’un sac à dos. Celle-ci est réalisé en matériaux recyclés récupérés afin d’encourager une économie circulaire. Le prototype est passé par plusieurs phases allant de la structure dépliante, fixe ou gonflable. J’ai finalement opté pour la structure fixe qui était plus en adéquation avec le matériau choisi et revu certaines contraintes qui étaient trop ambitieuses pour le temps imparti de cet exercice. La majorité de la chaise a été confectionnée avec du plastique recyclé HDPE (Polyéthylène haute densité) fondu et façonné à l’aide de moules en bois. Les différentes pièces moulées composant la chaise présentent des variations dans leurs courbures ce qui créent des tensions qui figent l’ensemble de l’assemblage. Les vides au sein de la chaise à bascule renvoient à un sentiment de légèreté. 

![](./images/Capture.JPG)  
![](./images/Capture2.JPG)  

![](./images/cache_prusa.JPG)  

## Recherches 


La première phase fut la récolte de plastique. On le sait aujourd’hui le plastique pollue énormément notre planète.  L’idée est donc d’en récupérer pour le recycler.
 Ici j’ai récolté du plastique HDPE (Polyéthylène haute densité) (triangle avec un 2) c’est un plastique dit thermoplastique c'est à dire qu'il se ramolie lorsqu'il est chauffé à une certaine température.  

![](./images/unnamed.jpg)  

 Il n’a pas été difficile d’en trouvé car on le retrouve partout. Il m’a fallu moins d’une semaine pour récolter plus de 10 kg.  

 ![](./images/ezgif.com-gif-maker__5_.gif)  

Le but est de fondre tout ce plastique et de lui donner « une seconde vie » 
Pour ce faire j’ai dû découper le tout en lamelle pour pouvoir en mettre une certaine quantité dans le four mais également pour que ça fonde plus facilement.  

![](./images/capuchon.jpg)
![](./images/CUTY.jpg)  


Pour m’aider j’ai regardé des vidéos youtube expliquant les étapes à suivre pour fabriquer des objets fait de plastique recyclé. La chaine s’appel [One Army](https://www.youtube.com/watch?v=Pp4vmfVlm2k&ab_channel=OneArmy
https://community.preciousplastic.com/) connu aussi sous le nom de precious plastic.
 
#### Test plastique  

Pour ce test j’ai choisi de réalisé une brique en plastique.
J’ai d’abord du fabriquer un moule carré en bois récupéré d’une dimension de 13x13 cm.  

![](./images/vido.gif)  

J’ai fait fondre le plastique dans le four du fablab muni d’un plat en métal que m’a prêté Axel et de papier cuisson.  

 ![](./images/four__1_.jpg)
![](./images/cuicui__1_.jpg)
![](./images/plasto__1_.jpg)

J’ai procédé par palier pour voir comment le plastique réagissait.
D’abord, j'ai mi le four à une température de 180° pendant 20 minutes. Le plastique était encore assez rigide. 
Après j’ai augmenté à 190° puis laisser pendant 10 minutes de plus. 
Le dernier palier était 200° (température maximum du four) pendant une durée de 20 minutes. J’ai obtenu une matière modelable.
Juste après il a fallu mettre le plastique fondu encore chaud dans le moule. Pour éviter que le plastique n’adhère aux parois j’ai disposé du papier cuisson dans le moule. 
Ensuite il a fallu presser la matière chaude avec des serres joint.  

![](./images/couvert__1_.jpg)![](./images/serre__1_.jpg)![](./images/moule__1_.jpg)

J’ai dû attendre environ 30 minutes pour que le tout refroidisse puis j’ai démoulé la brique. En refroidissant le plastique se contracte et perd du volume ce qui facilite le démoulage.  

![](./images/brick__1_.jpg)  

Pour accélérer le refroidissement j’ai plongé la brique sous l’eau froide.  

![](./images/eau__1_.jpg)  

Vu que j’ai compressé le moule en utilisant des serres joint on peut voir des imperfections au niveau des différentes faces. Le mieux pour évier cela est d’utiliser une presse hydraulique pour répartir la pression uniformément.  

![](./images/imperfection__1_.jpg)  

J’ai donc poncé la brique pour avoir quelque chose de plus lisse et esthétique.  

![](./images/ponce__1_.jpg)  

 ![](./images/brique_pon.jpg)  
 
Après ce petit test j’ai pu me lancer dans le dessin de la chaise. J’ai commencé par des croquis que j’ai repris sur autocad pour avoir un fichier convertible pour les machines du fablab.

![](./images/autocadgif.gif )

Le dondobag possède un siège et un dossier formés par des lattes pouvant se replier et s’emboiter. Ainsi, il est possible de le transporter facilement à la manière d’un sac à dos. La couverture en toile plastique vient entourer la structure avec une partie fixe et une autre partie retenue par du velcro qui donne la possibilité d’accéder à l’intérieur du sac mais également pour libérer les lattes pivotantes. L’utilisation du plastique permet d’avoir un objet solide et étanche.  

#### Test de la toile plastique  

Le second essaie était la confection de la toile qui allait me servir d’enveloppe pour le sac à dos. J’ai donc récupéré des vieux sacs poubelles que j’ai combiner en les pressant à chaud afin d’avoir un plastique plus résistant. La méthode utilisée est la même que pour le prototype 3. La seule différence ici est que je n’ai pas assemblé plusieurs morceaux de sacs plastiques mais les sacs en entier.
![](./images/sacp.jpg)  

J’ai de nouveau procédé par palier de température.
1)	115° + pression pendant 20 secondes : les sacs ne collaient pas entre eux
2)	120° + pression pendant 20 secondes : les sacs collaient mais pas entièrement
3)	130 ° + pression pendant 20 secondes : les sacs collaient parfaitement entre eux  

![](./images/press1.jpg)![](./images/press2.jpg)  


Une fois pressé on obtient une texture plus rigide et un rétrécissement de la matière.  

![](./images/fibre.jpg)  

Je suis satisfait du résultat, j’ai obtenu une matière souple mais solide qui servira d’enveloppe étanche au sac. Les patrons seront réalisés plus tard et découpé au laser.  

#### Réalisation des moules  

Le premier test de la brique était plutôt réussie la prochaine étape est la réalisation des moules pour les différentes pièces de la chaise pieds, siège, dossier)
Tous les moules ont été fabriquer avec du bois MDF d’épaisseur 18 mm et 6 mm que l’on trouve dans le commerce. 
Le premier moule que j’ai fabriqué est celui des pieds courbés du fauteuil. Ne connaissant pas la résistance du plastique j’ai dû fonctionné par déduction en prenant comme référence ma brique de plastique et en prenant compte les dimensions des plaques MDF. Le temps était compté j’ai donc opté pour une épaisseur de 24 mm (légèrement en dessous de l’épaisseur de la brique) pour être sûr de la résistance vue que les pieds allaient porter toute la charge.  

![](./images/shapy.jpg)
![](./images/scan.jpg)  

Deux découpes reprenant la même forme ont été effectué avec la shaper (mèche 6mm) sur une planche MDF de 122x61 cm – 18 mm et 6 mm d’épaisseur.  

![](./images/tacé.jpg)
![](./images/cutcut.jpg)  

Ensuite il a fallu poncer, vernir et coller l’ensemble pour éviter que le plastique n’accroche lors du démoulage.  

![](./images/verni_1.jpg)
![](./images/colle_1.jpg)
![](./images/colle_2.jpg)  

Une petite astuce donnée par Laurens était de percer des trous pour facilité le démoulage en poussant par le dessous du moule.  

![](./images/trourie.jpg)  

Le moules des lattes ont été fabriqué suivant le même procédé à partir d’une planche MDF de 18mm.  

#### Test à petite échelle  

À la suite de la discussion eu lors de la correction il m’a été demandé de réaliser un modèle réduit afin d’anticiper les problèmes que je pourrais rencontrer lors de la conception. J’ai donc fabriqué un prototype en bois découpé à la laser.  

![](./images/bois_2.jpg) ![](./images/bois1.jpg)  

#### Réduction 
Après une longue réflexion j'ai décidé de simplifier le projet trop ambitieux pour le temps dont je disposais. La réalisation se limitera à un chaise à bascule fait de plastique recyclé. En effet, suite à la correction et au test effectué je me suis rendu compte que certaines choses ne marchaient pas. Notament la jonction entre les lattes lorsqu'on les replies et le poids de l'ensemble beaucoup trop lourd pour être transporté comme un sac à dos.

#### Fabrication des pièces  

Il fallait déterminer la quantité de plastique nécessaire pour remplir tout le moule. J’ai utilisé le four du Fablab qui monte à une température maximum de 200°C ce qui était suffisant pour fondre le HDPE.  

![](./images/ plastoc.jpg)  

La température du four était réglée à 200°C et la matière était chauffé pendant 20 minutes.
Le plastique se déformait en fondant et donc prenait moins de place. Il a fallu ajouter des doses de matière au fur et à mesure.
J’ai utilisé du papier cuisson pour éviter que le plastique n’adhère au moule. Il s’avère que le plastique ne collait pas au moule mais au papier. Après quelques manipulations celui-ci se déchirait et n’était pas facile à récupérer.  

![](./images/papier.jpg)  

Muni de mes gants j’ai pu rassembler et modeler la matière fondue et encore chaude. Au-delà de 5 minutes le plastique commence à durcir et devient beaucoup moins maniable.  

![](./images/compact.jpg)  

Il était indispensable que tout le plastique soit à même température, du même type (ici HDPE) et encore chaud lors du placement dans le moule. Les plastiques ne fondent pas tous à la même température. Si les morceaux de plastiques avaient une différence de température il y avait le risque qu’ils ne fusionnent pas entre eux. 

Le four du Fablab à malheureusement surchauffé et n’a pas supporté la température qui s’était élevée à 240° dû aux plats et au plastique qu’il contenait.

J’ai finalement trouvé un autre four pour continuer. Cependant le temps de reprendre là où j’en étais la masse déjà fondu a eu le temps de refroidir et donc de durcir. Il a fallu le triple du temps pour parvenir à refondre le tout.  

 ![](./images/four.jpg)  

Le déroulement des opérations de moulage a pu reprendre. 
Les étapes de la manipulation consistaient à : 
1)	Placer la matière dans un au four à 225°C pendant 12 minutes
2)	Sortir le plat modeler et presser le plastique et lui donner une forme de boudin
3)	Remettre le plat au four pendant 8 minutes pour que le plastique refonde et fusionne en gardant une forme allongée pour le placer plus facilement dans le moule
4)	Sortir la matière du four et la placer dans le moule 
5)	Étirer et presser le plastique afin qu’il épouse la forme du moule
6)	Placer la plaque servant à presser 
7)	Placer les vis dans les trous et serrer les boulons à l’aide d’une clé et d’une visseuse pour obtenir une compression maximum
8)	Renforcer la pression à l’aide des serre-joints 
9)	Attendre 6 minutes, coffrage fermé, que le plastique prenne forme et refroidisse
10)	Ouvrir le coffrage laissé refroidir 5 minutes
11)	Démouler

Comme je ne possédais pas de presse hydraulique pour presser le plastique fondu afin qu’il se répartisse uniformément dans le moule il a fallu percer des trous supplémentaires pour accueillir des vis passant par la plaque du moule et une planche faisant office de presse. La compression de la matière se faisait par le serrage des boulons et à l’aide de serre-joints.  

![](./images/moumou.jpg)
![](./images/moule1.jpg)  

Je me suis d’un tourne vis et d’une marteau pour démouler facilement en utilisant les trous situé en dessous du moule.  

 ![](./images/tourne.jpg)  

J’ai obtenu la bonne forme mais avec quelques imperfections du au pressage « maison » et du surplus de matière qu’il était possible de rectifier par la suite en découpant et en ponçant l’élément.  

![](./images/pied1.jpg)  

Il a fallu découper le surplus de plastique et ensuite peser la pièce pour connaitre la quantité de plastique nécessaire pour mouler le deuxième pied. 
Le poids était de 1800 grammes.  

![](./images/gramme.jpg)  

J’ai dû renforcer le moule avec des clous car vu que le MDF est composé avec plusieurs plaques de fibres agglomérée lors du démoulage certaines parties ont été endommagés et se sont relevées. Il est aussi arrivé que le bois craque ou s’effrite à cause de la pression exercée par les serre joints ou le serrage des vis.
L’utilisation de moules en métal aurait été plus adéquat cependant je ne possédais pas les outils pour en fabriquer.  

![](./images/clou.jpg)

Le deuxième pied s’est présenté avec un écart. Il se pourrait que lors du placement de la matière cette partie ait été trop étiré et se soit déchiré.  

![](./images/prob.jpg)  

La solution pour rectifier cela fut simple. J’ai refondu du plastique que j’ai ensuite appliqué sur la zone. Le problème n’a pas affecté la solidité de la pièce. 

Le procédé a été répété pour fabriquer les lattes  

![](./images/moumou2.jpg)
![](./images/moule2.jpg)
![](./images/rresult.jpg)
![](./images/surplus.jpg)  


#### Assemblage  

J’ai utilisé un tuyau de 15mm de diamètre en métal servant à relier toutes les pièces ensemble et à contreventer la chaise. 
Il fallait d’abord percer les lattes et les pieds. J’ai donc découpé des stencils à la laser pour savoir précisément ou faire le trou.  

![](./images/stencil.jpg)  

Je les ai ensuite placés sur les différentes pièces.  

![](./images/plaçage.jpg)  

Le perçage ne s’est pas fait en une fois pour ne pas endommager le plastique. Il était nécessaire d’utiliser des mèches de différents diamètres.  

![](./images/mèche.jpg)
![](./images/trouou.jpg)  

Ensuite pour les pièces suivantes il suffisait de les superposer et de faire une légère marque.  

![](./images/superpo.jpg)  


L’étape suivante était de passer le tuyau dans les trous.  

 ![](./images/assss.jpg)  

Le fauteuil prenait forme petit à petit.  

![](./images/do.jpg)  

Pour éviter que les lattes ne bouges j’ai ajouté des éléments discrets pour les tenir en places.  

![](./images/ajout.jpg)  


Pour terminer j’ai coupé les morceaux de tuyaux qui dépassaient et poncer le tout.

 Pour une meilleure finition j’ai imprimé en 3D des éléments pour cacher les trous apparents.  
 
![](./images/cache.jpg)


![](./images/Canva_Option_Design.pdf)

