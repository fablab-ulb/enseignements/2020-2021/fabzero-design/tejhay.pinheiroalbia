# Choix de l'objet  

## DONDOLO ROCKING CHAIR CESARE LEONARDI & FRANCA STAGI
![](../images/archive.jpg)  
**Cesare Leonardi** est un architecte italien né en 1935. Il s’intéresse de près aux performances de la matière, aux structures et aux formes lors de ses études à l’école d’architecture de Florence. Il ne s'intéresse pas seulement à l’architecture, il explore d’autres domaine comme : la photographie, le design, l'urbanisme, la sculpture et la peinture. En 1963, il crée un bureau d’architecture à Modène avec **Franca Stagi** elle aussi architecte diplômée en architecture à l’université polytechnique de milan. Ils design ensemble le fauteuil à bascule appelé **Dondolo**. Celui-ci fut l’objet de nombreuses expositions. Il apparait notamment au MoMA à New York, Albert Museum à Londres, le Vitra Design Museum à Weil am Rhein et aujourd’hui au ADAM à Bruxelles. 

Le fauteuil Dondolo a été imaginé par Cesare Leonardi & Franca Stagi en 1967. 
La chaise longue représente une seule pièce moulée ne nécessitant pas d’assemblage d’une hauteur de 80 cm, une largeur de 42 cm et une longueur de 171cm.
Le siège et le repose pied ne sont pas en contact direct avec le sol ce qui donne, du au vide sous le siège, une impression de flotter. A cela s’ajoute une sensation de légèreté dû à la finesse de l’ensemble. Sa forme courbée permet de nous balancer, c’est comme une rocking chair revisitée avec un matériau moderne.  
Le tout est composé en fibre de verre. Cette matière malléable offre une plus grande liberté dans la conception d’objets par rapport aux matières traditionnels tel que le bois. La composition permet de réaliser une conception légère en économisant de la matière tout en étant très résistante. De plus, elle est peu couteuse à produire, s’entretient facilement et possède des propriétés tel que : résistant au feu, robuste et imperméable. La présence de détails tel que les sillons ou les renforts discrets présents sur les bords ne sont pas anodins. En effet, ils permettent de rigidifier l’ensemble et procurent une meilleure stabilité.
La principale fonction de cette chaise est de permettre l’assise cependant elle peut aussi servir d’élément décoratif que l’on retrouve à l’extérieur comme à l’intérieur. Le vide présent dans le bas peut éventuellement servir de rangement.


# Cours GitLab & Fusion 360 

## GITLAB  

Le cours de **GitLab** donné par **Denis Terwagne** était assez dense en informations. Je pensais comprendre au début mais je me suis perdu vers la fin des explications. 
C'était comme apprendre une nouvelle langue seulement ici c'était du codage : **Markdown**. Les différentes formations que je vais suivre vont me permettre d'apprenre de nouvelles compétences grace à la théorie mais aussi grace à la pratique. Je vais par la suite exécuter une série d'exercice réparties sur les cinq modules. Ce site va me permettre de vous partager mes différentes expériences en documentant tout mes faits et gestes que ce soit dans le but d'aider l'un ou l'autre qui rencontrerait le même problème que moi et pour m'aider moi-même car j'ai une mémoire limitée. Je décrirai mon apprentissage en détaillant les différentes étapes mes réussites ainsi que mes erreurs.


### Editer sa page Web avec GitLab

Le langage Markdown est utilisé pour éditer sa page web. J'ai d'abord essayé de décrypter les différents codages en comparant la page d'exemple en _mode Write_ et la page en _mode Preview_.
En observant les différents caractères utilisés et en ayant suivi le cours ce matin, on sait déjà qu'on utilise les signes : *, #, _, >. J'ai quand même suivi des tutoriels pour me rafraichir la mémoire.
C'est différents signes sont utilisés dans le langage **Markdown** qui est un codage utilisant des **balises** léger créé en 2004 par John Gruber avec l'aide d'Aaron Swartz. Contrairement au HTML il est plus simple à la lecture mais aussi en écriture. Il est toujours possible de convertir le Markdown en HTML grâce à Pandoc.

#### Etapes à suivre pour modifier sa page web :    

* Se rendre sur le site de [gitlab](https://gitlab.com/) : cliquer sur son projet **->** `repository` **->** `files` **->** `docs` **->** `index.md` **->** `edit`  

>Gitlab permet de modifier sa page web en utilisant le **Markdown**. Le Markdown est un langage de balises avec une écriture plus lisible et facile  que l’HTML.

* J’ai suivi les tutoriels [Markdown](https://www.markdowntutorial.com/) qui expliquent les différentes options que l'on retrouve dans un 
texte (italic, gras, entête, lien, insérer une image, citation, liste et paragraphe).   
Le site [ionos](https://www.ionos.fr/digitalguide/sites-internet/developpement-web/markdown/) apporte deux ou trois informations en plus et est en français. Les différentes options sont reprises dans le tableau ci-dessous. 

#### Tableau récapitulatif

|Option |Caractère |
| ------ | ------ |
| Italic | _ x _ |
| Gras| **  x  ** |
| Entête| # x  |  
| Insérer une image avec lien| ! [x] (lien de l'image)  |
| Citation | > x |
|Liste avec point | * + espace |
|Liste avec chiffre   | 1. 2. 3.|
| Paragraphe | double espace en fin de phrase |

>Gitlab permet d'éditer en appliquant directement une des options (italic, gras, etc.). Cependant il peut y avoir des bugs il est donc préférable d'encoder le tout manuellement.

Il est possible d'avoir un aperçu en cliquant sur `Preview`  
  
Pour revenir en mode _Edit_ cliquer sur `Write`
 
* Lorsque les modifications de votre texte sont terminées cliquer sur `Commit changes` pour appliquer les modifications à la page web.
* Pour voir la page de son site web : aller dans les `settings` **->** `pages` **->** cliquer sur le _lien de son site_

### Ajouter & compresser une image 
#### Etapes à suivre pour importer une image sur GitLab : 
* Aller dans `files` **->** `docs` **->** `images` **->** cliquer sur le **`+`** **->** `upload file` 
* Dans l'éditeur encoder : ![ ] (images/nomdufichier.jpeg)

> J'ai fait l'erreur de ne pas de mettre "images" avant la barre oblique donc la photo n'apparaissait pas.

>Je n'ai pas réussie à télécharger Graphics Magick j'ai donc utilisé Photoshop
#### Etapes à suivre pour compresser une photo sur Photoshop  
La dimension initiale de mon image importée est de : 1365x1365 = 5,3 Mo  
Utilisation de **photoshop** pour compresser l'image **en format pour le Web**  
`Fichier` **->** `Exporation` **->** `Enregistrer pour le web` **->** _Ajustez les paramètres_ **->** `Enregistrer`  

Fichier compressé obtenu : 400 x 400 = 126 Ko

J'ai préféré mettre des gifs plutot que des vidéos car ils s'activent directement et tournent en boucle tandis que les vidéos apparaissent mais ne se lisent si on appuie pas sur play.
 Voici un [tuto](https://www.youtube.com/watch?v=u0U5JtEtjHA&ab_channel=RebeccaCarpenterPhotography) qui explique comment faire un gif. Suivez le jusqu'à 2:08 min. Ensuite je vous conseil une exportation en format Web.

#### Etapes pour exporter en format Web : `fichier` **->** `Exporation` **->** `enregistrer pour le Web`  

Une fenêtre s'ouvre : _sélectionnez_ GIF (format de fichier optimisé) puis _changez la taille de l'image_ si nécessaire
Pour `otpions de boucle`: _sélectionnez_ Toujours  
Pour finir enregistrez  


## CLE SSH
Il nous a été demandé de faire une clé SSH afin de cloner sa page sur son ordinaeur. Possèdant un PC qui tourne avec Windows 10 il fallait que j'installe une série de programmes comme : Git, Shell Bash et Linux. Contrainte que les utilisateurs de Mac n'ont pas car ils ont directement accès au terminal. J'ai d'abord décidé de travailler directement sur Gitlab ce qui revenait au même.
J'ai quand même tenté de faire cette clé SSH. Pour ce faire j'ai cherché sur le Git des autres étudiants voir si quelqu'un était dans la même situation que moi. J'ai trouvé la solution sur le Git de Clementine qui a bien détaillé les étapes pour les utilisateurs de Windows.  
J'ai suivi d'abord la première étape qui était de se mettre en _mode développeur_.  
Un message d'avertissement est apparu.  
 ![](../images/message.JPG)    
 Suite à celui-ci "j'pense la question était vite répondue" ![](../images/quetion.gif)  
  
 J'ai décidé de ne pas continuer pour ne pas prendre le risque d'endommager mon ordinateur.  

## FUSION 360
Le cours de fusion était plus simple que celui sur GitLab. Ayant déjà utilisé des logiciels de modélisation 3D comme Sketchup, la partie conception de Fusion 360 était assez intuitif.Je vous invite à vous diriger vers le  [module 2](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/tejhay.pinheiroalbia/modules/module02/) pour en savoir plus.  
Suivre les tutoriels de **Thibaut Baes** pour connaitre les bases du logiciel Fusion 360 :   
[Partie 1](https://web.microsoftstream.com/video/c9e3743c-6094-442a-b774-78d94bdfc386)  
[Partie 2](https://web.microsoftstream.com/video/66a278b5-48ec-4198-bed6-0bd0a863efb6)

 



