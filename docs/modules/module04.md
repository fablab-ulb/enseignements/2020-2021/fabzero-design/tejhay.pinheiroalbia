# Utilisation du laser cutter

## ![](../images/lasersaure_logo.jpg) Découpe laser
Ce jeudi nous avons eu la formation du laser cutter avec **Axel Cornu**. Il nous a parler du fonctionnement de la machine, des matériaux qu’on pouvait utiliser, ceux interdits et des règles de sécurité. Je vous retape tout en dessous. Vous pouvez retrouver toutes ces informations sur les [PDF 1](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/files/LASER_cutter.pdf) et [PDF 2](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/files/Manuel_Lasersaur.pdf) d’Axel.

Après ce petit cours il nous a fait une démonstration en live.   
L’exercice a ensuite été de réaliser une **lampe** en lien avec notre objet. Il fallait d’abord tester en **maquette papier** et ensuite en utilisant des feuilles de **polypropylène** qu’on ferait découper par le lasersaure.  

Je me suis posé devant mon carnet avec ma feuille de papier et mon impression 3D. J'ai commencé à dessiner les premières choses qui me venait en tête en voyant le fauteuil. Après une heure j'ai trouvé quelque chose qui me plaisait et qui pourrait fonctionner.  
J'ai essayé de travailler avec un système d'encoches et de fentes.
J'ai réalisé un premier test en papier en faisant une partie de la lampe sachant que c'était un élément qui se répèterait.

![](../images/carnet.jpg)
![](../images/maquette_papier.jpg)

En papier ça fonctionnait plutot bien. Cependant, le polypropylène se comporte autrement car il est beaucoup plus **rigide**. La consigne était aussi qu'on ne pouvait pas utilser d'autres éléments que le polypropylène.  
J'ai redessiné les différentes pièces de ma lampe sur **autocad**.   

![](../images/autocad.jpg) 


# Laser cutter
Il y a deux modèles de laser cutter à notre disposition : la **lasersaure** et la **fullspectrum** (plus petite)

![](../images/lasersaur.jpg) ![](../images/full.jpg)  

### Lasersaur 
C'est une machine **open-source** qui possède une surface de découpe de 122 x 61 cm une hauteur de 12 cm. Sa vitesse maximum atteint les 6000 mm/min et a une puissance laser de 100 W. Le type de laser est un tube CO2 infrarouge. Celle-ci est relié à un ordinateur fixe sur lequel se trouve le logiciel **Driveboardapp**. L'application lit les fichiers vectoriels en SVG ou DXF qu'elle lance la commande au lasersaure  


### Fullspectrum
Elle a une surface de découpe plus petite 50x30 cm d’une hauteur de 6cm. Sa puissance laser est d 40W. Le type de laser est un tube Co2 également avec un pointeur rouge. Contrairement à la lasersaur qui utilise driveboardapp la fullspectrum utilise **retina engrave**. Cela permet de lire des fichiers vectoriels en **SVG** mais aussi des images matricielles c’est-à-dire **BMP, JPEG, PNG,TIFF ou des PDF**. 

  

## Règles de sécurité   
**Lasersaure**  
* Il faut vérifier que le matériau qu’on utilise ne fasse pas partie des matériaux déconseillés ou interdits. Les matériaux interdits peuvent provoquer dela fumée ou de la poussière nocives pour notre santé ou bien abimer la machine en fondant.
* Il est important de mettre en marche la vanne d’air comprimé et l’extracteur de fumée pour ne pas enfumer la pièce.
* En cas de problème appuyez sur le bouton d’arrêt d’urgence. (Le gros bouton rouge avec les flèches blanches)
* En cas de feu utilisez un extincteur au CO2.  


**Full spectrum**  
Les règles de sécurité sont les mêmes que pour la lasersaure excepté qu’elle est reliée à une pompe à air et non à une vanne d’air comprimé. Elle est dotée de boutons marche et arrêt qui apparaissent lors de la découpe.  
  
### Différents types de matériaux   

|Recommandés|Déconseillés |Interdits|
|--------|--------|--------|
|Bois contreplaqu (plywood)  |  MDF | PVC
Acrylique  |  ABS,PS| Cuivre
Papier, carton |   PE,PET,PP| Téflon
Textiles   |    Composites à base de fibres   | Vinyle, similicuir
||  Métaux | Résine phénolique  

## Découpe laser
Pour lancer une découpe on utilise l’application **Driveboardapp** ! [](../images/driveboardapp.JPG) présent sur l’ordinateur du fablab. Le fichier doit soit être en **SVG** soit en **DXF**.
Pour obtenir un fichier DXF j'ai dessiné sur **autocad** et ensuite exporter en DXF. 
On peut aussi utiliser **Inkscape**. Il peut convertir les pixels des images et bitmaps en lignes vectorielles. Une fois terminé on exporte en fichier SVG.
Pour utiliser le fichier DFX ou SVG dans Driveboard il fautglissé dans la fenêtre de l'application.

## Driveboard App
Il faut attribuer aux couleurs des lignes une **vitesse** (F), une **puissance** (%) et les mettre dans l'**ordre** de découpe (pass 1,2,3,...). Ainsi on peut choisir de simplement **graver** ou **couper**.  
Le mieux est de programmer la coupe en commençant avec les tracés **intérieurs** puis ceux **extérieurs**. En effet, si la découpe commence par l'extérieur il y a un risque que la pièce devienne **instable**.
Afin de paramétrer plus facilement la vitesse et la puissance du laser des tests ont été réalisé sur de plaquettes pour mieux visualiser les résultats.  
![](../images/ref.jpg)  

## Étapes pour lancer une découpe 
1)	Tourner le **bouton d’arret d’urgence** dans le sens des flèches blanches  
![](../images/boutoj.jpg)   
2)	Faire retourner la tête à l’origine en cliquant sur l’**icône maison** ![](../images/origine.JPG)  
3)	Ouvrir notre fichier avec le bouton **Open**. ![](../images/open.JPG)  Cela ne fonctionne pas sur le navigateur Microsoft Edge. En cas d’erreur exporter le dessin avec inkscape.
4)	Mettre son matériau dans la machine en s’assurant qu’il fasse parti des matériaux recommandés.
5)	Relever la tête de la **lentille** en dévissant la roulette rouge sur le coté. 
6)	A l’aide de l’application déplacer la tête du laser pour qu’elle se place au-dessus de votre matériau avec les boutons **Move et Jog**.![](../images/move_jog.JPG)   
7)	Régler la distance focale avec le support de **15 mm**. Il y a une roulette rouge sur le coté de la tête du laser qui permet de la monter et descendre à la bonne hauteur.
 ![](../images/15mm.jpg)       
  ![](../images/teteuh-rouletteuh.jpg)      
8)	Grâce au bouton **run bounding box** ![](../images/run_bounding.JPG) il est possible de voir si le laser dépasse du matériau. La tête effectuera le périmètre du dessin sans activer le laser.  
9)	Sélectionner une **couleur** avec le bouton `+`. Il est important de mettre les couleurs dans l'ordre de la découpe.   
![](../images/pass.JPG)    
10)	Régler la **vitesse** de découpe dans la case `F`  
11)	Régler la **puissance** de découpe avec le `%`  
Pour ajouter des étapes cliquez sur **More pass** ![](../images/more_pass.JPG)  
Des planches découpées servant d’exemple sont à disposition pour avoir un aperçu.    
Il y a également des exemples sur https://github.com/nortd/lasersaur/wiki/materials 
12)	Mettre en marche le **refroidisseur à eau** bouton noir   
![](../images/refroid.jpg)   
13)	Allumer l’**extracteur d’air**  
![](../images/extra.jpg)   
14)	Ouvrir la **vanne d’air comprimé**   
![](../images/comprim.jpg )   
15)	Lorsque le bouton **statuts** est vert ![](../images/status.JPG) cliquez sur le bouton **run** ![](../images/run.JPG). S’il est **rouge** la découpe ne se lancera pas.
Il est possible de mettre en **pause** ou d’**annuler** grâce aux boutons pause et stop. ![](../images/pause_stop.JPG)
Il est conseillé de garder un oeil sur la machine durant le procédé. Le trajet du laser s'affiche sur l'écran montrant les mouvment du laser en tant réel.  En cas de problème, appuyez sur le bouton d'arret d'urgence.

### Première découpe

> J'ai fait l'erreur de ne pas mettre de couleurs dans mon dessin. Sans couleur la machine ne suivra pas d'ordre et découpera tout pareil. 

Ensuite je n'ai pas eu trop de mal à utiliser la machine car Gwendoline était présente et m'a aidé pendant le processus. Sachant que l'engin pouvait prendre feu je n'étais pas serein. Un grand merci à elle ! Elle m'a également aidé à convertir mon fichier en SVG sur Inkscape car ma version n'était pas adéquate.

J'ai donc corrigé mon erreur en ajoutant trois couleurs auxquelles j'ai associé une vitesse F de 1500 et une puissancede 46 %. Si j'avais eu des éléments à graver j'aurais mi 20 %. Merci à Elodie pour les infos. 
Avant de lancer l'opération je me suis assuré que l'ordre de découpe était bon puis j'ai lancé le programme.  


Lorsque la découpe laser est terminée il est préferable d'attendre un moment que la fumée se dissipe avant d'ouvrir la vitre de protection.  

J'ai donc ici ma lampe en pièces détachées. 

![](../images/décou.jpg) 
![](../images/out.jpg) 

Montage de la lampe + test   
![](../images/poly.jpg)  ![](../images/dondolamp_gif.gif) 

### Deuxième découpe  
Je trouvais que ma première lampe ne correspondait pas assez avec le fauteuil Dondolo car elle était trop massive et elle nécessitait un assemblage de pièces tout le contraire du fauteuil.  
Ce que je recherchais c’était plus la finesse, la légèreté, le mouvement, un motif et un jeu d’ombre avec la lumière.   
J’ai donc fait des petits tests en papier puis en polypropylène avec les chutes de ma première découpe.  
 ![](../images/test-uno.jpg ) ![](../images/test-dos.jpg)  
Pour ma deuxième découpe j’ai tracé des spirales sur autocar que j’ai répartie sur différents calques de couleurs. Ici j’avais 3 couleurs, bleu, jaune et noir. J’ai alterné les couleurs en faisant une spirale sur deux.  
>J’ai exporté mon fichier en PDF pour l’importer dans Illustrator afin de l’exporter en SVG. Pour être sûr que ça fonctionne je l’ai ouvert sur Inkscape puis enregistrer sur ma clé USB en SVG à nouveau. Toutes ces étapes n’étaient pas nécessaires.  
  

J’ai connecté la clé puis ouvert le fichier dans DriveBoard l’échelle de mon dessin était beaucoup trop grande. En lançant run bounding box le laser faisait le trajet à l’extrémité maximum du cadre de découpe et je n’avais pas de feuille propylène aussi grande.  
Je remercie Alizée de m’avoir aidé pour cette partie. Elle a repris mon fichier sur Inkscape qu’elle a remis à la bonne échelle. J’ai enregistré la nouvelle version en SVG.  
J’ai attribué les différentes couleurs, vitesse et puissance comme pour la première découpe puis j’ai cliqué sur Run. (Les autres appareils : extracteur de fumé, refroidisseur, etc. était déjà allumés)  
![](../images/color-past.jpg)  ![](../images/écran-drive1.jpg)  

> Tout ne s’est pas passé comme prévu. Premièrement j’ai dessiné des lignes trop rapprochées les unes des autres ce qui donnaient un résultat beaucoup trop fin et donc beaucoup trop souple. Deuxièmement, vu que j’ai alterné les couleurs en faisant une spirale sur deux, le laser repassait près de lignes déjà coupées. Celles-ci étant déjà coupées ne tenaient pas en place et chevauchait les parties pas encore coupées. Par conséquent le laser recoupait dedans.  


Le mieux est de programmé le chemin du laser pour que sa découpe commence par les lignes intérieures pour finir avec les lignes extérieures. 
J’ai laissé le processus se faire jusqu’à la fin. Voici le résultat. 
![](../images/try2c.jpg) ![](../images/try2.jpg)   
![](../images/try21c.jpg) ![](../images/try-21.jpg)   

### Troisième découpe  

Finalement c’était un bon test ! Je sais maintenant que je dois attribuer une couleur différente pour chaque ligne en partant de l’intérieur vers l’extérieur. Je devais normalement avoir 18 couleurs différentes pour mes 18 spirales. Etant daltonien je n’étais pas sûr de pouvoir toutes les distinguer d’où mon choix de faire une spirale sur deux. Mauvaise idée…  

J’ai aussi remarqué à la fin de mon impression que l’écart entre mes spirales était très fin ce qui rendait le tout un peu trop souple. J’ai donc décidé d’augmenter l’écart entre chaque spirale pour avoir quelque chose de plus rigide. Ainsi le nombre de spirales est passé à 9 il fallait donc que je détermine 9 couleurs ce qui rendait la tâche plus facile pour mes yeux défectueux.   
Je suis repassé par autocad pour modifier le projet pour ensuite refaire les mêmes manipulations qu’au début. Un nouveau problème sinon ce n’est pas drôle ! Driveboard était aussi devenu daltonien et ne voyait que la couleur noire.   
Heureusement Hélène est venue à ma rescousse et après de longues péripéties elle parvint à trouver LA solution !   
Si on reprend dans l’ordre il suffisait d’enregistrer son fichier autocad en DXF pour ensuite l’ouvrir sur Inkscape où elle m’a aussi aidé à convertir mon texte pour garder que les contours et qu’il ne soit plus lu comme un bloc. Puis l’enregistrer en SVG.  
Il ne restait plus qu’à l’ouvrir sur driveboard. Un demi-miracle s’est produit, le fichier apparaissait avec les couleurs, à la bonne échelle mais n’avait toujours pas pris le texte en compte. C’est un mystère qu’il faut encore résoudre.   
Vous connaissez la suite, mettre les différentes couleurs dans l’ordre de découpe souhaité. Toujours de l’intérieur vers l’extérieur. Ajuster la vitesse et la puissance. Et bien sûr allumer les machines. RUN.   
![](../images/color-past2.jpg) ![](../images/écran-drive2.jpg)  
![](../images/laser_mov.gif)   

Voilà ce que j’obtiens au final     
![](../images/final-face.jpg)  ![](../images/final-sous.jpg)  



## Thermoformeuse formbox Mayku

Ce jour la j'ai eu la chance d'apprendre le fonctionnement de la thermoformeuse avec Laurens. Merci à lui d'avoir pris le temps de m'expliquer les différentes étapes.  

C'est une machine permettant de faire des moules avec des feuilles de plastique.  
![](../images/thermo.jpg)  
Étapes :  
-	Relier la machine à l’aspirateur   
-	Relevé les poignées sur les deux côtés pour remonter la partie supérieure du cadre  
-	![](../images/poignée.jpg)  
-	Poser une feuille plastique au centre du rectangle  
-	Rabaisser la première partie du cadre   
-	Redescendre les poignées   
-	Remonter la partie inférieure et supérieure du cadre qu’il est possible de bloquer au sommet de la machine  
Des résistances se trouve au-dessus permettant de faire chauffer la feuille plastique   
-	Régler le temps en tournant le bouton gauche et la température à l’aide du bouton droit  
![](../images/bouton.jpg)  
La température est indiquée sur l’emballage des feuilles en plastique (ici température : 5) toujours repasser par 0 pour que ça fonctionne.   
-	Régler le temps de chauffe (ici 2 min 20 sec) (généralement mettre le double de ce qui est indiqué)   
Une lumière clignote pour signaler que la machine chauffe lorsque la température sera atteinte la lumière sera fixe et verte  
-	Placer l’objet que vous voulez mouler sur la grille au centre   
-	Appuyer sur start (l’appareil va se mettre à chauffer)  
Une lumière commence à clignoter de plus en plus vite pour indiquer que la feuille plastique est assez souple pour mouler l’objet.  
La feuille va commencer à se détendre devenir flexible puis former un creux.  
-	Allumer l’aspirateur  
-	Quand la feuille est prête, prendre les deux poignets pour rabaisser les cadres vers l’objet  
L’aspirateur va aspirer et la feuille plastique va prendre la forme de l’objet  
-	Éteindre la machine   
-	Relever les poignées remonter le cadre supérieur  
-	Attendre quelques secondes que ça refroidisse  
-	Prendre le moule  


## ![](../images/silhouette.jpg) Silhouette cameo  

Aujourd'hui Gwendoline a utilisé la silhouette cameo, j’ai sauté sur l’occasion pour qu’elle m’apprenne comment l’utiliser. Merci encore pour la demonstration.
![](../images/cameo-ma.jpg) 


1)	Mettre sa clé USB dans la tour  
2)	Ouvrir le logiciel silhouette studio qui fonctionne avec la machine  
le logiciel lit les fichiers JPEG ou DXF  
3)	Glisser le ficchier dans silhouette studio  
4)	Cliquer sur le papillon 5eme case ne partant du haut  
5)	Sélectionner la zone de traçage  
6)	Délimiter la zone de traçage  
7)	Sélectionner si on veut la partie solide ou seulement les contours  
8)	Threshold pour définir la zone de découpe  
9)	Tracé  
10)	Send  
11)	Aperçu des ligne d découpe en rouge  
12)	Supprimer l’image   
13)	Sélectionner le matériau à découper  
14)	Définir une force et une vitesse + le nombre de passage  
Sur la machine   
1)	S’assurer que la machine est reliée à l’ordinateur par câble ou wifi  
2)	Découper le matériau qu’on veut utiliser au format de la planche quadrillée adhésive de la machine  
3)	Coller le matériau sur la planche quadrillée adhésive  
4)	Prendre en compte la flèche sur la planche qui indique de quelle coté on insère la planche dans la machine   
5)	Faire correspondre la planche avec les repères (triangles) sur la gauche de la machine  
6)	Appuyer sur LOAD pour faire rentrer la planche  
7)	Il y a possibilité de déplacer la planche ou la tête de découpe avec la molette grâce à des flèches sur l’écran  
8)	Revenir en arrière  
9)	Cliquer sur SEND   









