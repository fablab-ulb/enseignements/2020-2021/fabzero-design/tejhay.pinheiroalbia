# Utilisation de l'imprimante 3D  


##  ![](../images/prusa_logo.png ) Prusa sclicer
Le cours de ce matin sur prusa slicer été donné par **Gwen Best** et **Hélène Bardijn**. 

L’étape suivante était d’**exporter** son fichier **fusion 360** en fichier **stl** pour qu’il puisse être lu par **prusa slicer**.  
Exporter en fichier stl : 
`fichier` ![](../images/fichier.JPG) **->** `exporter` **->** _nommer le fichier_ **->** _choisir type de fichier_ "Fichiers STL (*.stl)" **->** _choisir l'emplacement du fichier_ **->** `Exporter`  

En ouvrant le logiciel on tombe sur le plateau de l’**imprimante 3D**. Celui-ci nous aide à visualiser notre objet. Pour importer son objet dans prusa slicer il suffit de faire **glisser** son fichier stl dans la fenêtre du logiciel.  
 ![](../images/plate.JPG) ![](../images/expert.JPG) 

Quand l'objet se retrouve sur le plateau il est encore possible de le manipuler afin de bien le placer. Sur la gauche se trouve des commandes pour déplacer, redimensionner, pivoter, positionner sur la surface et couper la modélisation.  
![](../images/manip.JPG ) 

Des **paramètres d’impression** par défaut son déjà programmés. Néanmoins il faut en **modifier** quelques-uns en fonction de l’imprimante utilisée et du projet. Il est important de se mettre en **mode expert** pour avoir accès à toutes les fonctions.  

## Réglages d’impression de base vu en cours: 

`Réglages d’impression`    

![](../images/couches_et_périmètres.JPG)    

**Hauteur de couche**     
_Hauteur de couche_ : **0,2** mm    
la buse est de 0.4 mm donc il faut prendre la moitié    

**Parois verticales**  
_Périmètres_ : **3** (minimum)  

**Coques horizontales**   
_couches solides_ : Du haut : **3**    Du bas : **3**   

**Qualité (découpage plus lent)**
Détecter les périmètres faisant des ponts -> **cocher** `V`   


![](../images/remplissage.JPG)    
**Remplissage**  
_Densité de remplissage_ : **ne doit pas dépasser 35 %**  
_Motif de remplissage_ : **choisir** parmi les différentes motidfs proposés en sachant que Nid d'abeille et gyroide sont les plus solides
Le remplissage sert à rigidifier ou rendre souple l'objet.  

![](../images/jupe_et_bordure.JPG)    
**Jupe**     
_Distance de l'objet_ : **3** mm    
_Largeur de la bordure_ : **2** mm   
Les bordures servent à solidifier la pièce pour éviter qu'elle tombe.
La distance de l’objet doit être supérieure à la largeur de la bordure pour éviter qu’elles se chevauchent.  

![](../images/supports.JPG)  
**Supports**  
_Générer des supports_ -> **cocher** `V`   
_Espacement du motif_ : **4** mm mais cela peut dépendre de l’objet   
_Espacement du motif d’interface_ : **2** mm mais cela peut dépendre de l’objet   

Lorsque le paramétrage d'impressionest terminé il est possible d'avoir un simulation d'impression en cliquant sur `découper maintenant` pour avoir un **aperçu** de la pièce à imprimer avec les supports. En baissant la **flèche orange** il est possible de voir le **processus d’impression** ainsi que les différentes couches où chaque couleur correspond à un type de foncionnalité. Sur la **droite de l'écran** on retrouve des **informations** sur la position du modèle en fonction des axes X,Y, et Z. Mais aussi sur la taille, le nombre de face, le filament et la durée d'impression.Ainsi on peut voir s'il y a des erreurs dans le paramétrage.  
 Une fois que tout est bon on peut `exporter le G-code` vers la **carte SD** de l'imprimante. Il ne faut pas oubliéde créer un **dossier** à son nom contenant le fichier d'impression pour pouvoir le retrouver facilement.    


## Imprimante 3D
**Modèle "Original Prusa i3 MK3"**
![](../images/prusa_slicer.jpg)  

Il est conseillé de **nettoyer** la surface du plateau à l’**acétone** pour que le fil plastique adhère mieux lors de la formation de la base. Il faut donc garder un oeil sur son impression les premières minutes pour voir si tout se passe bien.

Il faut bien regarder le type de **fil utilisé** pour pouvoir l’indiquer à la machine généralement on utilise du **PLA**. On peut retrouver le infos sur le fil sur le coté de la bobine. Le PLA nécessite une température de la buse à 215° et 60 ° pour le plateau.

## Lancement de l'impression

Lorsqu’on remet la carte SD dans le moniteur il suffit grace au knob (la roulette) de sélectionner son dossier puis son fichier pour démarrer l’impression.
![](../images/dossier.jpg)  ![](../images/fichier.jpg)    


Il faut attendre que la machine chauffe.  
![](../images/chauffe.jpg)  


Lorsqu’elle est prête elle démarrera son calibrage puis tracera une ligne sur le bord gauche du plateau pour enlever le surplus de fil de l’impression précédente.   
![](../images/marque.jpg)  

Lorsque l'impression commence, l'écran indique différentes informations  
![](../images/num.jpg)  
1.  Knob
2.  Reset
3.	Température de la buse 
4.	Température du plateau d'impression
5.	Progression de l'impression 
6.	Barre d'état : Prusa i3 MK3S prêt. + nom_fichier.gcod
7.	Position de l'axe Z
8.	Vitesse d'impression
9.	Estimation du temps d'impression

Si on remarque une anomalie durant l'impression il est possible de la mettre en pause. Pour cela il faut appuyer sur le knob, sélectionner `Pause print`. Pour relancer l'impression la ou elle s'est arreter, appuer sur le knob puis `Resume print`. Pour stopper l'impression appuyer sur `Stop`. En stoppant il n'est pas possible de reprendre l'impression là où on l'a laissé. 
![](../images/pause.jpg)
![](../images/resume.jpg)  
Merci à Cassandra pour le tuyau.

## Changement de fil

Je voulais absolument que ma reproduction 3D soit de couleur blanche comme l’objet original. Pour cela j’ai du changer la bobine de l’imprimante.  
Je remercie Laurens qui m'a aidé pour cette étape.

![](../images/fil-blanc.jpg)    


**Pour enlever le fil** : 
1)	Presser le knob (la roulette)
2)	Unload filament
3)	Regarder le type de fil, ici c’était du PLA 
4)	Attendre que la machine chauffe 
5)	Un bip sonore nous indique quand la température est atteint 
6)	Presser sur le knob pour enlever le fil  

**Pour remettre le fil** :
1)	Appuyer sur le knob 
2)	Couper le bout du fil en biais
3)	Charger le filament 
4)	Vérifier que la machine a bien pris le fil 
5)	La machine va faire descendre du fil il vaut vérifier si la couleur du fil sortant correspond à la couleur de la bobine 
6)	Si non presser non et attendre que la couleur soit correcte 
7)	Si la couleur est correcte pressez oui


### Prototype 1
Ma première impression est un succès !  
Pour retirer son impression il suffit de prendre le plateau et de le plier légèrement pour que la pièce se décolle. Ensuite il ne reste plus qu'à nettoyer la pièce en enlevant les supports.

![](../images/gif_3D.gif)

![](../images/0Findimpression.jpg) ![](../images/1-impression-fin.jpg) ![](../images/2-impression-fini-.jpg)


### Prototype 2

`Réglages d’impression`    

![](../images/couches_et_périmètres.JPG)    

**Hauteur de couche**     
_Hauteur de couche_ : **0,2** mm    
  
**Parois verticales**  
_Périmètres_ : **2** (minimum)  

**Coques horizontales**   
_couches solides_ : Du haut : **7**    Du bas : **5**    

**Qualité (découpage plus lent)**
Détecter les périmètres faisant des ponts -> **cocher** `V`   


![](../images/remplissage.JPG)    
**Remplissage**  
_Densité de remplissage_ : **15** % 
_Motif de remplissage_ : **Nid d'abeille 3D**  

![](../images/jupe_et_bordure.JPG)    
**Jupe**     
_Distance de l'objet_ : **3** mm    
_Largeur de la bordure_ : **2** mm   

![](../images/supports.JPG)  
**Supports**  
_Générer des supports_ -> **cocher** `V`   
_Espacement du motif_ : **4** mm   
_Espacement du motif d’interface_ : **0,2** mm 

Pour mon deuxième essai mon objet était beaucoup trop grand j’ai donc dû le rétrécir avec l’outil redimensionner pour qu’il soit adapté au plateau de l’imprimante 3D mais également pour éviter que l’impression prenne trop longtemps.  

![](../images/c1.png) 

![](../images/proto2.jpg) ![](../images/proto_2.1.jpg)   


La deuxième impression ne s'est pas passée comme prévue.

>Lors de l’impression tout allait bien jusqu’à la fin de l’impression le problème est survenu lorsque j’ai dû enlever les supports. Je n’arrivais pas à retirer entièrement les filaments plastiques. En plus de ça j'ai remarqué que la finition à l'extrémité du siège et du repose pied n'était pas faite comme prévu. Le problème venait du paramètre d’espacement du motif d’interface qui n’était pas assez large et de l'échelle qui était trop petite pour que l'imprimante fasse les détails.   
Un grand merci à Gwendoline pour la solution !




### Prototype 3  

`Réglages d’impression`    

![](../images/couches_et_périmètres.JPG)    

**Hauteur de couche**     
_Hauteur de couche_ : **0,2** mm    
  
**Parois verticales**  
_Périmètres_ : **3** (minimum)  

**Coques horizontales**   
_couches solides_ : Du haut : **3**    Du bas : **3**    

**Qualité (découpage plus lent)**
Détecter les périmètres faisant des ponts -> **cocher** `V`   


![](../images/remplissage.JPG)    
**Remplissage**  
_Densité de remplissage_ : **15** % 
_Motif de remplissage_ : **Nid d'abeille**   

![](../images/jupe_et_bordure.JPG)    
**Jupe**     
_Distance de l'objet_ : **3** mm    
_Largeur de la bordure_ : **2** mm   

![](../images/supports.JPG)  
**Supports**  
_Générer des supports_ -> **cocher** `V`   
_Espacement du motif_ : **4** mm   
_Espacement du motif d’interface_ : **2** mm  


J'ai modifier les paramètres d'espacement du motif d'interface qui était à 0,2 mm et maintenant à 2 mm sur prusa slicer. J'ai également agrandi ma pièce pour que les détails apparaissent. J'ai pu vérifier cela avec le bouton découper maintenant.  
![](../images/proto3.png) ![](../images/GIF.gif)    

L’impression s’est déroulé comme prévu, le problème a été résolu. 

## Reconstituion 3D

Après la modélisation du fauteuil Dondolo j'ai voulu me lancer dans un autre projet un peu plus personnel. Je me suis intéressé à la **reconstruction en 3D** en utilisant la technique de la **photogrammétrie**. Cette méthode est utilisée dans certains bureaux d’architecte pour réaliser la 3D d’un lieu avec des lasers scanners puissants.  

« La photogrammétrie est une technique qui consiste à effectuer des mesures dans une scène, en utilisant la parallaxe obtenue entre des images acquises selon des points de vue différents. » (wikipédia)  

Le but ici était de me prendre en **photo** sous différents **angles** afin de me reconstituer en trois dimensions.
Le premier programme que j’ai trouvé s’appelle **Meshroom**. C’est un logiciel **Open Source** qui reconstitue le **modèle** choisi et son **environnement**.  Celui-ci analyse les photos en 2D importées pour créer un **assemblage**.   
Je ne vais pas développer plus pour l’instant car après quelques étapes le logiciel arrête la modélisation. Je cherche encore à résoudre le problème. Celui-ci peut venir de la version que j’utilise, des photos que j’ai importées, du CPU (unité centrale de traitement) de l’échelle ou autres. Je dois encore explorer les solutions.  
Pour ceux que ça intéresse et qui aurait plus de chance que moi voici un tuto pour utiliser Meshroom. 
Tuto : https://www.youtube.com/watch?v=k4NTf0hMjtY&feature=emb_title&ab_channel=CGGeek  

J’ai quand même demandé à Gwendoline si elle ne connaissait pas par hasard le logiciel. Ayant réponse à tout ne sait-on jamais ! Effectivement elle eut la réponse, une encore meilleure que ce que j’attendais. 
« Pourquoi tu n’utilises pas le scanner 3D à la place ? »   
Je vais maintenant vous montrer comment utiliser le **Sense scanner 3D**  
Un grand MERCI à Gwendoline ! 

 ## Sense scanner 3D
Je vous passe les étapes pour installer le logiciel sur mon ordinateur, je n’ai pas réussi car le site officiel ne possédait plus le logiciel nécessaire. Heureusement il était déjà installé sur l’ordinateur présent au fablab.  
Ici le logiciel se sert du tracking par triangulation pour générer les formes de l’élément scanné.  

Le scan se fait à l’aide de cet appareil. Il est constitué de deux caméras et d’un laser qui évalue la profondeur. Il faut le brancher sur un ordinateur à un port USB 3.0.  
![](../images/scanner.jpg)

Pour la première utilisation il faut faire reconnaitre le Sense scanner sur le logiciel. Un code donné avec l'appareil sera demandé.  

![](../images/code.JPG)  

Etant donné que l’ordinateur du fablab est assez chargé il a tendance à bugger. 
La solution pour éviter cela est de lancer le programme avec l’appareil Sense scanner débrancher. Une fois le logiciel ouvert il faut passer la vidéo explicative et cliquer sur `reçu` pour passer les tutos. Dés que c’es fait on peut brancher l’appareil.  

Avant de commencer soyez sûr de pouvoir tourner à 360° autour de ce que vous voulez scanner, le Sense scanner doit rester relier à l’ordinateur tout au long du processus. 
L’image en direct s’affichera à l’écran comme une webcam.  

![](../images/cam.jpg)  

Pour lancer la numérisation cliquer sur `play` ![](../images/scan.JPG)   
Pour le mettre en pause `pause` ![](../images/bouton_pause.JPG)   
Pour terminer `terminer` ![](../images/terminer.JPG)  
Pour recommencer un scan `recommencer` ![](../images/recommencer_num.JPG)  
  

Sélectionner ce que vous voulez scanner : Un objet, la tête d’une personne ou une personne entière. ![](../images/objet_corps.JPG)    

La reconstitution se fait en direct sur l’ordinateur.  

* L’écran indique l’avancer de la numérisation. Les éléments apparaissant en vert signalent les parties prisent en compte par l’appareil et transmis au logiciel.  

* Si vous allez trop vite un message vous demandant de ralentir apparaitra à l’écran.  

* Il se peut que la machine perde le suivi à ce moment-là un message vous avertira et le scan apparaitra en rouge, il suffit de vous repositionner.  

* Une fois que la majorité du modèle est vert cliquez sur `terminer`.  

Voici le résultat 
![](../images/result.jpg)   

L’étape suivante sert à modifier sa numérisation avec les outils suivant : rogner, couper, effacer, solidifier, couleur. N’étant pas parfaite certains trous peuvent être corrigés. ![](../images/edit_num.JPG)   

![](../images/trou.jpg)    
 
   
Réparation :  
![](../images/reparation.JPG)    
-	Solidifier : forme un bloc, et uniformisant le tout    
![](../images/solid.jpg)       
-	Surface : permet en repassant sur un trou de le combler     
-	Remplissage : permet de reboucher les trous en une fois en reliant les surfaces entre elles    
![](../images/bouchage.jpg)  

Couleur : 
![](../images/color.JPG)   
-	Réglage de la luminosité 
-	Réglage du contraste 

Lorsque vous êtes satisfait du résultat cliquez `appliquer`  
En cliquant sur `terminer` vous pouvez :   
![](../images/exportt.JPG)   
- Exporter en format WRL, STL, PLY, ou OBJ.  
- Partager sur facebook  
- Partager sur sketchfab   
- Imprimer  

Tuto : https://www.youtube.com/watch?v=5rxNiQmRheE&t=313s&ab_channel=3DPrint-TechDesign  

Je n'ai pas encore eu la possibilité de l'imprimer mais voici un petit aperçu sur Prusa Slicer.
![](../images/aperçu.jpg) 




