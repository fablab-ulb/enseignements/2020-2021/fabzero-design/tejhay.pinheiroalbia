# Utilisation de la shaper tool  

## ![](../images/shaplo.jpg)  


La machine se présente avec des poignées qui permettent de diriger la découpe.  

![](../images/shaper.jpg)  

Elle possède un moniteur muni d’une caméra qui permet de voir le trajet de la découpe  

![](../images/écran.jpg) ![](../images/camera.jpg)  

Grace à un ruban domino qu’on applique sur la surface de travail la machine va pouvoir créer des repères dans l’espace où on peut placer notre dessin.  

![](../images/ruban_domino.jpg)  

La découpe se fait grâce à une fraiseuse muni d’une mèche qui est branché à un terminal indépendant.
On retrouve sur la fraiseuse un bouton ON/OFF et une roulette pour régler la vitesse de rotation.  

![](../images/onoff.jpg)  

Avant de changer de fraise il faut veiller à débrancher la prise d’alimentation ainsi que celle reliant le moniteur à la fraiseuse. Il faut également vérifier que le bouton soit en position OFF (éteint)
Ensuite enlevez le plexi de protection muni d’aimants qui protège des projections de poussières ou de débris de découpe.  

![](../images/prise.jpg)  

Sur la droite de celle-ci se trouve une vis qu’il faut desserrer en utilisant la clé en T pour pouvoir retirer la fraiseuse. Pour changer la mèche il suffit de presser la partie noire sur le côté du cylindre et de tourner le boulon avec la clé 19 mm.  

![](../images/clé.jpg)  

Inséré la mèche et refaites les manipulations dans le sens inverse. 
La machine se dirige selon les axes X et Y. Avant de lancer sa découpe brancher le tuyau d’aspiration de la poussière.  

![](../images/aspiration.jpg)  

Un des premières étapes sera de placer la planche que vous voulez graver ou découper grâce à du scotch double face.  

![](../images/double_face.jpg)  

Pour placer la shaper il faut la soulever en utilisant la poignée prévue à cet effet  


Sur l’écran de la shaper : 
-	Touchez `Continuer` 
-	Connectez-vous au wifi si nécessaire
-	Placez le ruban domino sur la zone de travail (espacement d’environ 10cm)  

![](../images/disposition.jpg)  


-	Pointez la caméra vers le ruban domino
-	Faites un nouveau scan 
-	Appuyez sur le bouton vert (poignée droite) 
-	Faites glisser la machine (pas soulever) sur la surface de travail pour que la caméra crée une carte reprenant tous les dominos en bleus à l’écran 
-	Appuyez sur le bouton vert pour terminer 
-	Tapotez deux fois sur l’écran pour faire un zoom arrière 
-	Connectez la clé USB avec le fichier en SVG enregistré préalablement sur le côté gauche de l’appareil
-	Cliquez sur le `+` 
-	Sélectionnez le fichier pour le placer sur l’écran
-	Il est possible d’effectuer une rotation si nécessaire
-	Pour placer le dessin appuyez sur le bouton vert
-	Une ligne en traits tillés apparait et indique le sens à suivre pour la découpe
-	Allez dans CUT pour les paramètres 
-	En haut à droite de l’écran se trouve une jauge indiquant lorsqu’on ne respecte pas le tracé. Lors de la découpe pour éviter une erreur la mèche se relèvera si on est trop en dehors de la zone tracé et qu’elle ne peut pas corriger le débordement.
-	Pour aider il y a un cercle sur l’écran qui sert de repère. Il ne faut pas que notre tracé soit en dehors de ce cercle pour que la machine puisse effectuer une correction s‘il y a un décalage.
-	  (Réglage découpe)
Lorsque les réglages sont effectués appuyer sur le bouton vert 
-	Un point blanc cerclé de bleu apparait à l’écran
-	Il faut glisser la machine en passant par le tracé tout en gardant le point entre les quatre lignes et dans le cercle
-	La fraiseuse étant indépendante bouge pour corriger les mouvmenet imprécis
-	Il est possible en appuyant sur AUTO (le bouton vert) de donner l’ordre à la machine d’effectuer le trajet du tracé jusqu’à la limite du cercle sans bouger l’appareil
-	Sur l’écran s’affiche en bleu les lignes sur lesquels vous êtes déjà passé 
Différente coupe possible :
-	Allez dans le menu CUT 
-	Touchez la deuxième icone à gauche en partant du haut et sélectionner le mode de coupe (à l’intérieur de la ligne, à l’extérieur de la ligne, sur la ligne, pocket ou guide
-	Appuyez le bouton vert
-	Effectuez le trajet en glissant la machine  


Il y a des vidéos tutoriel reprenant tout ce que j’ai cité sur le site de [Shaper Tool] (https://www.shapertools.com/en-us/)
