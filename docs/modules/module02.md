# Conception assistée par ordinateur
 
## ![](../images/logo-fusion.jpg) FUSION 360
Pour la conception de notre objet j'ai utilisé le logiciel **Fusion 360** celui-ci nous permet de **modéliser des objets en 3D**.  

On travaille en fonction d'**axes horizontaux : (X,Y) et vertical (Z)**. 
* Pour commencer il suffit de cliquer sur `créer une esquisse` ![](../images/esquisse.PNG).  
* Trois plans placés selon les différents axes sont donnés. Il faut sélectionner celui qui convient pour tracer son dessin.![](../images/plans.PNG )

* Une barre d’outils offrant différentes fonctionnalités de dessin on à la possibilité de tracer des lignes droites ou courbes ou directement générer une forme comme un cercle ou un rectangle.![](../images/barre_outil.PNG)  
Pour plus de précisions on peut encoder les mesures manuellement ainsi qu’ajouter des contraintes. 
Lorsque l’esquisse est terminée il faut cliquer sur `terminer l'esquisse`.  

* Il est possible une fois la forme voulue obtenue de la modifier en solide grâce aux fonctions extruder, révolution, balayage, lissage, congé, chanfrein, etc. Ces différents outils permettent de sculpter les formes.  
![](../images/barre_outil_2.PNG) 

* Les différentes vues, esquisses et paramètres du document sont répertoriés dans des calques en haut à gauche. Il y a également une timeline qui permet de revenir sur des manipulations.  
![](../images/calque.PNG) ![](../images/timeline.JPG) 

Les différents points illustrés au-dessus montrent la base des outils du logiciel vu en cours. Il est possible d'accéder à plus de paramètres. Je ne vais pas illustrer chaque paramètres pour éviter de surcharger la page.  
 
>Ma première erreur ici fut d’installer la version d’évaluation de fusion 360 qui ne permet pas d’enregistrer ses modélisations ce qui rend impossible  Vérifié bien d’avoir télécharger la version complète de fusion cela vous évitera de devoir recommencer votre travaille.   

### Prototype 1
Pour ma première réalisation j’ai voulu faire au plus simple et me limité à la forme de l’objet.  
Le but ce jour-là était de me familiariser avec ce nouvel outil d’impression 3D. J’ai donc inséré une image du fauteuil à bascule de profil dans fusion 360. Je l’ai placé sur le plan avant de l’axe X.  

Pour insérer une image :
cliquer sur `insérer` **->** `insérer à partir de mon ordinateur...` **->** _sélectionner son image_ **->** `ouvrir`**->** _placer sur le plan voulu_ **->** _Ajuster l'image(pivoter, glisser,agrandir)_  **->** `Enter`

![](../images/1.jpg)  
 J’ai commencé une nouvelle esquisse sur le même plan. Puis, à l’aide de l’outil **spline** qui permet de faire des **courbes** j’ai retracé la partie extérieure de la chaise mais pas l’entiereté.  
![](../images/2.jpg)  
Pour créer l’épaisseur j’ai simplement sélectionné la ligne puis utilisé l’outil **décalage** pour qu’il n’y ai pas de différence dans la courbe. Terminer l’esquisse.  
![](../images/3.jpg)    
Pour finir, il suffisait d’**extruder** en entrant la largeur de la chaise pour obtenir la forme principale du fauteuil.  

Pour voir le résultat de l'impression 3D vous pouvez vous rendre au [Module 3](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/tejhay.pinheiroalbia/modules/module03/)

>Lorsque j'ai voulu ajouter des photos et vidéos dans ce module 2 les images n'apparaissaient pas car j'ai oublié d'encoder deux point avant après la première parenthèse  
Il faut écrire (sans tenir compte des espaces) :  ! [ ] ( **..** / images / 3 . jpg )  

### Prototype 2

Cette fois-ci j’ai redessiné avec le logiciel **autocad**, d’après une coupe du fauteuil avec cotation dessiné par Cesare Leonardi.  

![](../images/cotation.jpg )  

Pour importer un fichier DWG (autocad) dans fusion 360 : `fichier`**->** `ouvrir` **->** `ouvrir à partir de mon ordinateur...`**->** sélectionner le _fichier_ **->** _attendre le chargement_ **->** `fermer`**->** `fichier`**->** `ouvrir` **->** _Admin project_ **->** _sélectionner son fichier_ **->** `ouvrir`
Une fois terminé j’ai **importé** le fichier sur fusion 360 puis replacé sur le plan de l’**axe Y**. Pour modéliser l’objet j’ai repris le dessin que j’ai replacé sur le plan de l’axe Y. Le fauteuil est creusé de sillons.   
1) Pour le réaliser j’ai copié le dessin plusieurs fois puis collé sur plusieurs esquisses parallèles espacées selon la largeur des rainures.  
2) J’ai d’abord **extrudé** ![](../images/extruder.JPG) ma première esquisse pour former le **corps principal** qui reprenait la mesure de la largeur du siège.  
3) Pour créer les sillons j’ai également **extrudé** les esquisses suivantes en reprenant la mesure indiquée sur les côtes. J’ai ainsi obtenu un corps principal et des **corps secondaires**.
4) J’ai ensuite dû utiliser l’outil **appuyer/tirer** ![](../images/appuyer_tirer.JPG) pour réduire la surface inférieure des corps secondaires. 
Cette manipulation permet d’éviter que lors de la combinaison des deux corps les sillons ne soient pas creusés jusqu’au bout comme sur le Dondolo.
5) Après ça j’ai utilisé l’outil **combiner** ![](../images/combiner.JPG) avec l’opération **couper**. Il faut choisir le corps principal en première sélection et un des corps secondaires en deuxième sélection pour que le sillon se soit découpé dans le bon corps. J’ai répété cette opération pour les autres corps.  
![](../images/couper.JPG )  


6) Pour finir j’ai utilisé l’outil **congé** pour arrondir les angles. ![](../images/congé.JPG)  

![](../images/e1.png) ![](../images/e2.png) ![](../images/e3.png) ![](../images/e4.png) ![](../images/e5.png)  ![](../images/rendu.jpg)  

A la fin j’ai appliqué un rendu pour avoir un effet plus réaliste et plus proche du visuel du fauteuil.

>J’ai fait l’erreur de tout travailler sur la même esquisse. Au bout d’une série de manipulations le fichier était devenu beaucoup trop lourd et fusion 360 plantait. 

