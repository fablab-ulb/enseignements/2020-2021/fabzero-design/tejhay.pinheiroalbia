## A PROPOS DE MOI  

Salut !  
Je m’appelle **Tejhay** je suis actuellement en **master 1** à l’ULB la cambre Horta architecture.  
Pour cette première année j’ai choisi l’option _Architecture & Design_ car elle offre l’opportunité d’apprendre à utiliser des logiciels de conception ainsi que des machines comme l’**imprimante 3D**, la **découpeuse laser**, la **CNC**, etc. Ceux-ci s’avèrent très pratiques dans la réalisation de projet.

   

![](images/compress2.jpg)

## A PROPOS DE MA VIE
Je suis né et j’ai grandi en Belgique dans la commune de Saint-Gilles, connu pour être un quartier multiculturel et artistique. Je me suis toujours intéressé de près ou de loin au design. Mon intérêt pour les arts et la création vient surement du trajet pour aller à l’école que j’ai effectué pendant une quinzaine d’année de la crèche à ma rétho. En effet, je passais par la rue Haute à l’aller et par la rue Blaes au retour, deux rues  remplies d’antiquaires et de galeries d’artistes, ce fut pour moi assez inspirant. 
Ayant toujours suivi des études dans l'enseignement général,je n'avais pas la possibilité de m'épanouir pleinement. C'est seulement au moment de choisir mes études universitaire que l'architecture s'est imposé à moi comme une évidence.
J’ai donc fait mes années de bachelier en architecture à l’ULB.




## A PROPOS DE MON CHOIX
J’ai choisi le **Dondolo de Cesare Leonardi & Franca Stagi**.
J’ai d’abord été attiré par la collection car contrairement aux autres estrades exposées au ADAM où l’on pouvait retrouver au moins une touche de couleur, celle-ci n’en avait pas. J’étais devant un ensemble de trois objets complètement monochromes. D’un côté il y avait la 'Nastro' Lounge Chair et de l’autre la kappa coffee table. L’un était trop strict et l’autre trop sobre ce qui ne correspondait à ma personnalité. C’est pour cela que mes yeux se sont portés sur l’élément centrale du podium qui étais le fauteuil à bascule Dondolo. La première chose qui m’a interpellé fut sa finesse et sa légèreté. En comparaison les premiers designs me paraissaient très épais et lourd visuellement. Ensuite il y avait ce côté très simple mais à la fois complexe de sa conception comme si elle avait été dessinée en un seul trait. J’ai également été attiré par sa forme courbée donnant une sensation de mouvement au sens figuré comme au sens propre lui donnant un aspect ludique. A cela s’ajoute le jeu de lumière qui accentue les parties creuses donnant de la texture grâce aux ombres et aux reflets.

![](./images/fauteuilpic.jpg)

Pour en savoir un peu plus sur son histoire dirigez vous vers le [Module 01](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/tejhay.pinheiroalbia/modules/module01/)

### 360°

![](./images/vidéo_musée.gif)





